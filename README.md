# Myth LootCouncil #

MLC is a LootCouncil addon for WildStar. It is intended to be used primarily by Myth members to distribute loot on raids, but can be used by anyone.

### Features ###

* Automated distribution process with custom timers
* Automated item linking, along with spec
* Autoloot feature for any item, to any looter
* Randomize all items
* One-click loot assignment

### Contribution guidelines ###

* If you have any ideas for improvements or found a bug, please open an issue.
* Bugs will be worked out ASAP, features might take longer to implement.

### All rights reserved. ###