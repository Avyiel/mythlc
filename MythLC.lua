-----------------------------------------------------------------------------------------------
-- MythLC - Archon Taucore <Myth>
-- Latest version: 1.4 (10/08/2017)
-----------------------------------------------------------------------------------------------

require "Window"
require "GameLib"
require "Item"

-----------------------------------------------------------------------------------------------
-- MythLC Module Definition
-----------------------------------------------------------------------------------------------
local MythLC = {}
local version = "1.4"

-----------------------------------------------------------------------------------------------
-- Constants
-----------------------------------------------------------------------------------------------
local allChannels = ChatSystemLib.GetChannels()
local activeChannel = ChatSystemLib.ChatChannel_Party
local testChannel = ChatSystemLib.ChatChannel_Say
local sysChannel = ChatSystemLib.ChatChannel_System

-----------------------------------------------------------------------------------------------
-- Locals
-----------------------------------------------------------------------------------------------
local tItemList = {}
local tLooterList = {}
local nLootIdx = 0
local uCurrItem = nil
local sCurrUnit = ""
local uLooter = nil

local bCurrDistributing = false
local bBeingDistributed = false
local bItemEquipped = false
local specRollSelection = "MS"
local lastSpec = ""
local lastItem = nil
local isMasterLooter = false


-----------------------------------------------------------------------------------------------
-- Data Tables
-----------------------------------------------------------------------------------------------
local tSettings = {
    bAutoLoot = false,
    sAutoLooter = "Looty McLooter",
    tAutolootItems = {
        "Divine Class Focus - Major",
    	"Divine Class Focus - Minor",
    	"Divine Set Focus - Major",
    	"Divine Set Focus - Minor",
    	"Pure Class Focus - Major",
    	"Pure Class Focus - Minor",
    	"Pure Set Focus - Major",
    	"Pure Set Focus - Minor",
    	"Datascape Matrix",
    	"Genetic Matrix",
    	"Tarnished Eldan Gift",
    	"Encrypted Datashard"
    },
    bAutoStopRoll = true,
    nAutoStopTime = 15,
    bDebug = false,
    bLocal = false,
    bFakeItems = false
}

local tStats = {
	[Unit.CodeEnumProperties.AssaultRating]              = {label = "Assault Rating",    short = "AP"}, --works
	[Unit.CodeEnumProperties.SupportRating]              = {label = "Support Rating",    short = "SP"}, --works
	[Unit.CodeEnumProperties.Rating_CritChanceIncrease]  = {label = "Crit Chance",       short = "CH"}, --works
	[Unit.CodeEnumProperties.RatingCritSeverityIncrease] = {label = "Crit Severity",     short = "CS"}, --works
	[Unit.CodeEnumProperties.RatingMultiHitChance]       = {label = "Multihit Chance",   short = "MH"}, --works
	[Unit.CodeEnumProperties.RatingMultiHitAmount]       = {label = "Multihit Severity", short = "MS"},
	[Unit.CodeEnumProperties.Rating_AvoidReduce]         = {label = "Strikethrough",     short = "STR"}, --works
	[Unit.CodeEnumProperties.Armor]                      = {label = "Armor",             short = "A"}, --works
	[Unit.CodeEnumProperties.RatingVigor]                = {label = "Vigor",             short = "V"}, --works
	[Unit.CodeEnumProperties.ShieldCapacityMax]          = {label = "Shield",            short = "SD"},
	[Unit.CodeEnumProperties.Rating_AvoidIncrease]       = {label = "Deflect",           short = "DC"}, --works
	[Unit.CodeEnumProperties.RatingGlanceChance]         = {label = "Glance",            short = "GC"}, --works
	[Unit.CodeEnumProperties.RatingCriticalMitigation]   = {label = "Crit Mitigation",   short = "CM"}, --works
	[Unit.CodeEnumProperties.PvPOffensiveRating]         = {label = "PvP Power",         short = "PvP PR"},
	[Unit.CodeEnumProperties.PvPDefensiveRating]         = {label = "PvP Defense",       short = "PvP DR"},
	[Unit.CodeEnumProperties.BaseFocusRecoveryInCombat]  = {label = "Focus Regen",       short = "FR"},
	[Unit.CodeEnumProperties.BaseFocusPool]              = {label = "Focus Pool",        short = "FP"},
	[Unit.CodeEnumProperties.BaseHealth]                 = {label = "Max Health",        short = "H"}, --works
}

local tItemQualities = {
	[Item.CodeEnumItemQuality.Inferior]  = {label = "Inferior",  color = "ItemQuality_Inferior"},
	[Item.CodeEnumItemQuality.Average]   = {label = "Average",   color = "ItemQuality_Average"},
	[Item.CodeEnumItemQuality.Good]      = {label = "Good",      color = "ItemQuality_Good"},
	[Item.CodeEnumItemQuality.Excellent] = {label = "Excellent", color = "ItemQuality_Excellent"},
	[Item.CodeEnumItemQuality.Superb]    = {label = "Superb",    color = "ItemQuality_Superb"},
	[Item.CodeEnumItemQuality.Legendary] = {label = "Legendary", color = "ItemQuality_Legendary"},
	[Item.CodeEnumItemQuality.Artifact]  = {label = "Artifact",  color = "ItemQuality_Artifact"},
}

local tClassFromId = {
	"Warrior",
	"Engineer",
	"Esper",
	"Medic",
	"Stalker",
	"",
	"Spellslinger"
}

local ktAutoLoot = {
	"Divine Class Focus - Major",
	"Divine Class Focus - Minor",
	"Divine Set Focus - Major",
	"Divine Set Focus - Minor",
	"Pure Class Focus - Major",
	"Pure Class Focus - Minor",
	"Pure Set Focus - Major",
	"Pure Set Focus - Minor",
	"Datascape Matrix",
	"Genetic Matrix",
	"Tarnished Eldan Gift",
	"Encrypted Datashard",
}

local tSpecList = {
	["os"]  	 = "OS",
	["os "] 	 = "OS",
	[" os"] 	 = "OS",
	["OS"]  	 = "OS",
	["OS "] 	 = "OS",
	[" OS"] 	 = "OS",
	["ms"]  	 = "MS",
	["ms "] 	 = "MS",
	[" ms"]	 	 = "MS",
	["MS"]  	 = "MS",
	["MS "]	  	 = "MS",
	[" MS"] 	 = "MS",
	["costume"]  = "C",
	["costume "] = "C",
	[" costume"] = "C"
}
-----------------------------------------------------------------------------------------------
-- Initialization
-----------------------------------------------------------------------------------------------
function MythLC:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    -- initialize variables here

    return o
end

function MythLC:Init()
    Apollo.RegisterAddon(self)
end

-----------------------------------------------------------------------------------------------
-- MythLC OnLoad
-----------------------------------------------------------------------------------------------
function MythLC:OnLoad()
    -- load our form file
	self.xmlDoc = XmlDoc.CreateFromFile("MythLC.xml")
	self.xmlDoc:RegisterCallback("OnDocumentReady", self)

    -- Register handlers for events, slash commands and timer, etc.
    Apollo.RegisterEventHandler("WindowManagementReady", "OnWindowManagementReady", self)
    Apollo.RegisterEventHandler("MasterLootUpdate",      "OnKeyDown", self)
    -- Apollo.RegisterEventHandler("LootAssigned",		     "OnLootAssigned", self)
    Apollo.RegisterEventHandler("ChatMessage", 	         "OnChatMessage", self)
    Apollo.RegisterEventHandler("ItemLink", 		     "OnItemLink", self)

    Apollo.RegisterSlashCommand("lc",      "OnSlashCommand", self)
    Apollo.RegisterSlashCommand("lctest",  "OnTestCommand", self)
    Apollo.RegisterSlashCommand("lcdebug", "OnDebugCommand", self)
    Apollo.RegisterSlashCommand("lclocal", "OnLocalCommand", self)
    Apollo.RegisterSlashCommand("lcdev",   "OnDevCommand", self)

    self.timeToLink = ApolloTimer.Create(15.0, false, "OnEndDistribution", self)
    self.timeToLink:Stop()
end

-----------------------------------------------------------------------------------------------
-- MythLC OnDocLoaded
-----------------------------------------------------------------------------------------------
function MythLC:OnDocumentReady()
	if self.xmlDoc ~= nil and self.xmlDoc:IsLoaded() then
	    self.wndMain = Apollo.LoadForm(self.xmlDoc, "MythLC", nil, self)
		if self.wndMain == nil then
			Apollo.AddAddonErrorText(self, "Could not load the main window for some reason.")
			return
		end

        self.wndItemList     = self.wndMain:FindChild("LinkedItemList")
	    self.wndLeftSide     = self.wndMain:FindChild("TooltipContainer")
	    self.wndHeaderBtns   = self.wndMain:FindChild("HeaderButtons")
	    self.wndStatusBar    = self.wndMain:FindChild("HeaderStatusBar")
	    self.btnDistribution = self.wndMain:FindChild("DistributionBtn")
	    self.wndInlineSpec   = self.wndMain:FindChild("InlineSpecSelect")
        self.wndSettings     = self.wndMain:FindChild("MainConfigWindow")

	    self.wndMain:Show(false, true)

		-- Do additional Addon initialization here
        self.wndInlineSpec:FindChild("MainSpecButton"):SetCheck(true)

        if tSettings.bDebug then self.wndSettings:FindChild("DebugButton"):SetCheck(true)
        else self.wndSettings:FindChild("DebugButton"):SetCheck(false) end
        if tSettings.bLocal then self.wndSettings:FindChild("LocalButton"):SetCheck(true)
        else self.wndSettings:FindChild("LocalButton"):SetCheck(false) end
        if tSettings.bAutoLoot then self.wndSettings:FindChild("AutoLootBtn"):SetCheck(true)
        else self.wndSettings:FindChild("AutoLootBtn"):SetCheck(false) end
        if tSettings.bAutoStopRoll then self.wndSettings:FindChild("DistributionTimerBtn"):SetCheck(true)
        else self.wndSettings:FindChild("DistributionTimerBtn"):SetCheck(false) end
        self.wndSettings:FindChild("AutolooterInput"):SetText(tSettings.sAutoLooter)
        self.wndSettings:FindChild("AutoStopRollTime"):SetText(tSettings.nAutoStopTime)

        self:RefreshAutolootList()
        self:CheckRaidAssist()
        self:OnWindowManagementReady()

        math.randomseed( os.time() )
	end
end

-----------------------------------------------------------------------------------------------
-- MythLC Load/Save
-----------------------------------------------------------------------------------------------
function MythLC:OnSave(eLevel)
    if eLevel ~= GameLib.CodeEnumAddonSaveLevel.Account then return nil end

    local tSave = {}

    tSave.bDebug = tSettings.bDebug
    tSave.bLocal = tSettings.bLocal
    tSave.bAutoLoot = tSettings.bAutoLoot
    tSave.sAutoLooter = tSettings.sAutoLooter
    tSave.bAutoStopRoll = tSettings.bAutoStopRoll
    tSave.nAutoStopTime = tSettings.nAutoStopTime
    tSave.tAutolootItems = {}

    for idx, sItem in pairs(tSettings.tAutolootItems) do
        tSave.tAutolootItems[idx] = sItem
    end

    return tSave
end

function MythLC:OnRestore(eLevel, saveData)
    if eLevel ~= GameLib.CodeEnumAddonSaveLevel.Account then return end

    if saveData.bDebug ~= nil then
        tSettings.bDebug = saveData.bDebug
    end
    if saveData.bLocal ~= nil then
        tSettings.bLocal = tSettings.bLocal
    end
    if saveData.bAutoLoot ~= nil then
        tSettings.bAutoLoot = saveData.bAutoLoot
    end
    if saveData.sAutoLooter ~= nil then
        tSettings.sAutoLooter = saveData.sAutoLooter
    end
    if saveData.bAutoStopRoll ~= nil then
        tSettings.bAutoStopRoll = saveData.bAutoStopRoll
    end
    if saveData.nAutoStopTime ~= nil then
        tSettings.nAutoStopTime = saveData.nAutoStopTime
    end

    tSettings.tAutolootItems = {}
    if saveData.tAutolootItems ~= nil then
        for i, item in pairs(saveData.tAutolootItems) do
            tSettings.tAutolootItems[i] = item
        end
    end
end

-- hooks up with CRB's window management system
function MythLC:OnWindowManagementReady()
    Event_FireGenericEvent("WindowManagementRegister", {strName = "Myth LC"})
	Event_FireGenericEvent("WindowManagementAdd", {wnd = self.wndMain, strName = "Myth LC"})
end

-----------------------------------------------------------------------------------------------
-- MythLC Chat Commands
-----------------------------------------------------------------------------------------------
-- on SlashCommand "/lc"
-- Toggles the main window
function MythLC:OnSlashCommand()
    self:OnMasterLootUpdate()
    self:CheckRaidAssist()
    if not self.wndMain:IsShown() then
	    self.wndMain:Show(true)
    else
        self.wndMain:Show(false)
    end
end

-- on SlashCommand "/lcdebug"
-- Enables verbose logging to ChatChannel_Debug
function MythLC:OnDebugCommand()
	tSettings.bDebug = not tSettings.bDebug
    if tSettings.bDebug then Print("Debug mode ON")
    else Print("Debug mode OFF") end
end

-- on SlashCommand "/lclocal"
-- Enable using the addon on ChatChannel_Say
function MythLC:OnLocalCommand()
    tSettings.bLocal = not tSettings.bLocal
    if tSettings.bLocal then Print("Local testing ON")
    else Print("Local testing OFF") end
end

-- on SlashCommand "/lcdev"
-- Developer testing, calls all helper functions
function MythLC:OnDevCommand()
	self:OnDebugCommand()
    self:OnLocalCommand()
    self:OnTestCommand()
end

-- on SlashCommand "/lctest"
function MythLC:OnTestCommand()

    tItemList = {}
	for i = 69886, 69891 do
		local uItem = Item.GetDataFromId(i)

		table.insert(tItemList, { itemDrop = uItem, nLootId = i, bIsMaster = true, tLooters = {} })
	end

    table.insert(tLooterList, GameLib:GetPlayerUnit())

	self.wndHeaderBtns:FindChild("DropdownListBtn"):SetText("MasterLooter Items [ " .. table.getn(tItemList) .. " ]")
    tSettings.bFakeItems = true

	self:dPrint("Test loot generated")
end

-----------------------------------------------------------------------------------------------
-- Settings Methods
-----------------------------------------------------------------------------------------------
function MythLC:OnToggleSettings( wndHandler, wndControl, eMouseButton )
    self:dPrint("Toggle settings button clicked")
    if wndControl:IsChecked() then
        self:dPrint("Opening settings")
        self.wndSettings:Show(true)
        self:RefreshAutolootList()
    else
        self:dPrint("Closing settings")
        self.wndSettings:Show(false)
    end
end

function MythLC:OnToggleDebug( wndHandler, wndControl, eMouseButton )
    if wndControl:IsChecked() then
        self:dPrint("Debug ON")
        tSettings.bDebug = true
    else
        self:dPrint("Debug OFF")
        tSettings.bDebug = false
    end
end

function MythLC:OnToggleLocal( wndHandler, wndControl, eMouseButton )
    if wndControl:IsChecked() then
        self:dPrint("Local testing ON")
        tSettings.bLocal = true
    else
        self:dPrint("Local testing OFF")
        tSettings.bLocal = false
    end
end

function MythLC:OnToggleAutoloot( wndHandler, wndControl, eMouseButton )
    if wndControl:IsChecked() then
        self:dPrint("Autoloot ON")
        tSettings.bAutoLoot = true
    else
        self:dPrint("Autoloot OFF")
        tSettings.bAutoLoot = false
    end
end

function MythLC:OnSetAutolooter( wndHandler, wndControl, strText )
    tSettings.sAutoLooter = strText
    self:dPrint("New AutoLooter: " .. tSettings.sAutoLooter)
end

function MythLC:OnAddAutolootItem( wndHandler, wndControl, strText )
    table.insert(tSettings.tAutolootItems, strText)
    wndControl:SetText("")
    self:dPrint("Added item: " .. strText)
    self:RefreshAutolootList()
end

function MythLC:OnRemoveAutolootItem( wndHandler, wndControl, eMouseButton )
    local sItem = wndControl:GetText()
    wndControl:Destroy()

    for i, _ in pairs(tSettings.tAutolootItems) do
        if tSettings.tAutolootItems[i] == sItem then
            self:dPrint("Removed item: " .. sItem)
            table.remove(tSettings.tAutolootItems, i)
        end
    end

    self:RefreshAutolootList()
end

function MythLC:OnResetAutolootItems( wndHandler, wndControl, eMouseButton )
    self.wndMain:FindChild("AutolooterItemList"):DestroyChildren()

    tSettings.tAutolootItems = {}
    for _, sItem in pairs(ktAutoLoot) do
        table.insert(tSettings.tAutolootItems, sItem)
    end

    self:dPrint("Autoloot list ist refreshed")
    self:RefreshAutolootList()
end

function MythLC:RefreshAutolootList()
    local tItems = tSettings.tAutolootItems
    if not tItems then return end

    local wndAutolootItemList = self.wndMain:FindChild("AutolooterItemList")
    wndAutolootItemList:DestroyChildren()

    for _, sItem in pairs(tItems) do
        local wndAutolootItem = Apollo.LoadForm(self.xmlDoc, "AutolootItem", wndAutolootItemList, self)
        wndAutolootItem:SetText(sItem)
    end

    wndAutolootItemList:ArrangeChildrenVert()
end

function MythLC:OnToggleAutoStop( wndHandler, wndControl, eMouseButton )
    if wndControl:IsChecked() then
        self:dPrint("Auto-stop roll ON")
        tSettings.bAutoStopRoll = true
    else
        self:dPrint("Auto-stop roll OFF")
        tSettings.bAutoStopRoll = false
    end
end

function MythLC:OnSetAutolootTime( wndHandler, wndControl, strText )
        local nTimer = tonumber(strText)
        tSettings.nAutoStopTime = nTimer
        self:dPrint("Set auto-stop timer to: " .. nTimer)
end

-----------------------------------------------------------------------------------------------
-- MasterLooter Methods
-----------------------------------------------------------------------------------------------
function MythLC:OnMasterLootUpdate()
--tMasterLoot contains the keys tLooters, itemDrop, nLootId, bIsMaster
	--tLooters: table with keys 1,2,3,etc contains Units viable for loot (in range)
	--itemDrop: contains Item that dropped
	--nLootId: individual item ID
	--bIsMaster}: true/false if you are masterlooter

	local tMasterLoot = GameLib.GetMasterLoot()

	tItemList = {}
    tLooterList = {}

	for idx, tMLItem in pairs(tMasterLoot) do
		table.insert(tItemList, tMLItem)

        for n, value in pairs(tMasterLoot[idx]["tLooters"]) do
			table.insert(tLooterList, tMasterLoot[idx]["tLooters"][n])
		end
	end

	self.wndHeaderBtns:FindChild("DropdownListBtn"):SetText("MasterLooter Items [ " .. table.getn(tItemList) .. " ]")

    self:CheckRaidAssist()
end

function MythLC:CheckRaidAssist()
	if GroupLib.AmILeader() or GroupLib.GetMemberCount() == 0 then
        self:dPrint(" User is leader or not in a group")
		self.wndStatusBar:Show(false)
		self.wndHeaderBtns:Show(true)
        bIsLeader = true
	else
        self:dPrint(" User is not leader")
		self.wndStatusBar:Show(true)
		self.wndHeaderBtns:Show(false)
        bIsLeader = false
	end
end

-----------------------------------------------------------------------------------------------
-- MythLC Form Button Functions
-----------------------------------------------------------------------------------------------
function MythLC:OnClose()
	self.wndMain:Close()
end

function MythLC:OnPrepDistribution( wndHandler, wndControl, eMouseButton )
    self:dPrint("DistributionStart button pressed")
    if uCurrItem ~= nil then
        self:dPrint("Preparing distribution")
		if tSettings.bLocal then
			allChannels[testChannel]:Send( "Distributing " .. uCurrItem["itemDrop"]:GetChatLinkString() .. " MS/OS" )
		else
			allChannels[activeChannel]:Send( "Distributing " .. uCurrItem["itemDrop"]:GetChatLinkString() .. " MS/OS" )
		end

	else
        self:dPrint("Current item nil")
		self.btnDistribution:SetCheck(false)
	end
end

function MythLC:OnEndDistribution( wndHandler, wndControl, eMouseButton )
    if tSettings.bAutoStopRoll then
        self.timeToLink:Stop()
    end

    if bCurrDistributing and tSettings.bLocal then
		allChannels[testChannel]:Send( "Ended!")
	else
		allChannels[activeChannel]:Send( "Ended!")
	end
end

-- Start of the Distribution
function MythLC:OnDistributionStart(strSender)
	if strSender ~= "" then

		self:dPrint("Starting distribution")

		bCurrDistributing = true

        if tSettings.bAutoStopRoll and bIsLeader then
            self.timeToLink:Set(tSettings.nAutoStopTime, true)
            self.timeToLink:Start()
        end

		self.btnDistribution:SetText("Stop Distribution by: " .. strSender )
		self.wndStatusBar:SetText("Current Distribution by: " .. strSender )
		self.btnDistribution:SetCheck(true)
		self.wndItemList:DestroyChildren()
	end
end

-- End of the Distribution
function MythLC:OnDistributionStop()
	self:dPrint("Ending roll")

	-- This would clean the left window, kinda optional.
	-- self.wndLeftSide:DestroyChildren()

	self.btnDistribution:SetText("Start New Distribution")
	self.wndStatusBar:SetText("No Distribution Active")
	self.btnDistribution:SetCheck(false)
	self:PopulateGearBtn()
	bCurrDistributing = false
end

function MythLC:OnOpenDropdown( wndHandler, wndControl, eMouseButton )
    local wndDropdown = self.wndMain:FindChild("DropdownContainer")
    local wndDropdownList = self.wndMain:FindChild("DropdownItemList")
    local wndEmptyText = wndDropdown:FindChild("EmptyText")

    wndControl:SetCheck(true)

    wndEmptyText:Show(false)
    wndDropdown:Show(true)
    wndDropdownList:DestroyChildren()

    if not tItemList then
        wndEmptyText:Show(true)
        return
    end

    for i, v in pairs(tItemList) do
        local tCurrItem = tItemList[i]["itemDrop"]

        local wndItem = Apollo.LoadForm(self.xmlDoc, "DropdownItem", wndDropdownList, self)
        wndItem:SetData({tItem = tItemList[i], nItemId = i})
        wndItem:FindChild("ItemIcon"):GetWindowSubclass():SetItem(tCurrItem)

        local wndItemLabel = wndItem:FindChild("Label")
        wndItemLabel:SetText(tCurrItem:GetName())
        wndItemLabel:SetTextColor(tItemQualities[tCurrItem.GetItemQuality()].color)
    end

    wndDropdownList:ArrangeChildrenVert()
end

function MythLC:OnDropdownHide( wndHandler, wndControl )
    self.wndMain:FindChild("DropdownListBtn"):SetCheck(false)
end

function MythLC:OnDropdownItemCheck( wndHandler, wndControl, eMouseButton )
    self:OnDropdownHide()
    self.wndMain:FindChild("DropdownContainer"):Close()
    wndControl:SetCheck(true)

    local tCheckedItem = wndControl:GetData()
    if tCheckedItem then
        nLootIdx = tCheckedItem["nItemId"]
        uCurrItem = tCheckedItem["tItem"]

        self:dPrint("Item selected for distribution: " .. tItemList[nLootIdx]["itemDrop"]:GetName() .. ", ID: " ..
                    tItemList[nLootIdx].nLootId)
        self:dPrint("Populating left area and gear button")

		self:PopulateTooltipContainer(uCurrItem["itemDrop"])
		self:PopulateGearBtn(uCurrItem["itemDrop"])
        self:PopulateLooterList()

    end
end

function MythLC:OnSpecButtonChange( wndHandler, wndControl, eMouseButton )
    if wndControl:IsChecked() then
        local strButtonName = wndControl:GetName()
		self:dPrint("" .. strButtonName .. " is checked")

		if strButtonName == "MainSpecButton" then
			specRollSelection = tSpecList["MS"]
		elseif strButtonName == "OffSpecButton" then
			specRollSelection = tSpecList["OS"]
		elseif strButtonName == "CostumeButton" then
			specRollSelection = "costume"
		else
			specRollSelection = tSpecList["MS"]
		end
	end
end

function MythLC:OnLinkItem( wndHandler, wndControl, eMouseButton )
    if bItemEquipped and bCurrDistributing and tSettings.bLocal then
        self:dPrint("Linking current gear to local chat")
		allChannels[testChannel]:Send( wndControl:GetData():GetChatLinkString().." "..specRollSelection )
	elseif bItemEquipped and bCurrDistributing then
        self:dPrint("Linking current gear")
		allChannels[activeChannel ]:Send( wndControl:GetData():GetChatLinkString().." "..specRollSelection )
    end
end

function MythLC:OnItemMouseover( wndHandler, wndControl, x, y )
    if wndControl:GetData() then
        local someItem = wndControl:GetData()

        if someItem["tItem"] then
            Tooltip.GetItemTooltipForm(self, wndControl, someItem["tItem"]["itemDrop"], {})
        else
            Tooltip.GetItemTooltipForm(self, wndControl, wndControl:GetData(), {})
    	end
    end
end

---------------------------------------------------------------------------------------------------
-- Item management methods
---------------------------------------------------------------------------------------------------
function MythLC:OnSetCurrentItem(uItem)
	if uItem ~= nil then
		self:dPrint("Populating left area and gear button")

		self:PopulateTooltipContainer(uItem)
		self:PopulateGearBtn(uItem)

        if bBeingDistributed then
            for idx, tItem in pairs(tItemList) do
                if tItem["itemDrop"]:GetItemId() == uItem:GetItemId() then
                    bBeingDistributed = false
                    nLootIdx = idx
                    uCurrItem = tItem["itemDrop"]
                    self:dPrint("Selected current item: " .. tItemList[nLootIdx]["itemDrop"]:GetName() .. ", ID: " .. tItemList[nLootIdx].nLootId)
                    break
                end
            end
        end

        -- Current item was already set, and it is only importtant for the masterlooter
    else
        self:dPrint("Clearing left area and gear button")

		self:PopulateTooltipContainer()
		self:PopulateGearBtn()

        -- Only needs to clear it when resetting everything
        uCurrItem = nil
	end
end

function MythLC:PopulateTooltipContainer(uItem)
    self.wndLeftSide:DestroyChildren()
	if uItem then
		Tooltip.GetItemTooltipForm(self, self.wndLeftSide, uItem, {bPermanent = true, wndParent = self.wndLeftSide, bNotEquipped = true})
	end
end

function MythLC:PopulateGearBtn(uItem)
    self:dPrint("Populating current gear button")
	local btnUserGear = self.wndMain:FindChild("CurrentGearBtn")
	local item = self:GetItemData(uItem)
	bItemEquipped = false

	if item then
		local equippedGear = GameLib.GetPlayerUnit():GetEquippedItems()
		if not equippedGear then return end

		for _, uItemInfo in pairs(equippedGear) do
			if item.eSlot == uItemInfo:GetInventoryId() then
                self:dPrint("Found item on the same slot")
                btnUserGear:Enable(true)
                bItemEquipped = true
				btnUserGear:SetData(uItemInfo)
				btnUserGear:FindChild("GearIcon"):GetWindowSubclass():SetItem(uItemInfo)
			end
		end
	end

	if not bItemEquipped then
        btnUserGear:Enable(false)
        self:dPrint("Nil item")
		btnUserGear:SetData({})
		btnUserGear:FindChild("GearIcon"):GetWindowSubclass():SetItem("")
	end
end

function MythLC:PopulateLooterList()
    self.wndItemList:DestroyChildren()

    if tLooterList ~= nil then
        self:dPrint("Populating looter list")
        for _, uLooterUnit in pairs(tLooterList) do
            local wndUnit = Apollo.LoadForm(self.xmlDoc, "LooterButton", self.wndItemList, self)
            local wndUnitName = wndUnit:FindChild("UnitName")
            local wndClassIcon = wndUnit:FindChild("ClassIcon")

            strClass = uLooterUnit:GetClassId()
            if strClass == 1 then
                strClass = "Warrior"
            elseif strClass == 2 then
                strClass = "Engineer"
            elseif strClass == 3 then
                strClass = "Esper"
            elseif strClass == 4 then
                strClass = "Medic"
            elseif strClass == 5 then
                strClass = "Stalker"
            elseif strClass == 7 then
                strClass = "Spellslinger"
            end

            wndClassIcon:SetSprite("Icon_Windows_UI_CRB_" .. strClass)
            wndUnitName:SetText(uLooterUnit:GetName())
            wndUnit:SetData(uLooterUnit)
        end
    end

    self.wndItemList:ArrangeChildrenVert()
end

function MythLC:ListItem(tSegment, spec, sender)
	-- List Item
	local uItem = self:GetItemData(tSegment.uItem)
	local wndItem = Apollo.LoadForm(self.xmlDoc, "LinkedItem", self.wndItemList, self)
	local wndItemText = wndItem:FindChild("ItemText")

	wndItem:SetData(item)
	wndItem:FindChild("ItemIcon"):GetWindowSubclass():SetItem(uItem.item)
	wndItemText:SetText(uItem.strName .. "\n"
						.. uItem.item:GetItemTypeName()
						.. " - iLvL: "
						.. uItem.nEffectiveLevel
    		            .. "\n" .. self.StatsString(uItem))

	wndItemText:SetTextColor(tItemQualities[uItem.eQuality].color)

	--List character
	local wndChar = wndItem:FindChild("CharacterName")
	local wndClass = wndItem:FindChild("ClassIcon")
	local wndSpec = wndItem:FindChild("Spec")

	wndChar:SetText(sender)
	wndSpec:SetText(spec)

	if tLooterList ~= nil then
		for i, value in pairs(tLooterList) do
			if sender == tLooterList[i]:GetName() then
				local strClass = tLooterList[i]:GetClassId()
				if strClass == 1 then
					strClass = "Warrior"
				elseif strClass == 2 then
					strClass = "Engineer"
				elseif strClass == 3 then
					strClass = "Esper"
				elseif strClass == 4 then
					strClass = "Medic"
				elseif strClass == 5 then
					strClass = "Stalker"
				elseif strClass == 7 then
					strClass = "Spellslinger"
				end

				wndClass:SetSprite("Icon_Windows_UI_CRB_" .. strClass)
			end
		end
	else
		wndClass:SetSprite("")
	end

	self.wndItemList:ArrangeChildrenVert()

	lastSpec = ""
	lastItem = nil
end

-- Gets item's stats, name and inventory slot
function MythLC:GetItemData(uItem)
	if uItem then
		local tItemInfo = uItem:GetDetailedInfo().tPrimary

		tItemInfo.item = uItem
		tItemInfo.eSlot = uItem:GetSlot()
		tItemInfo.tStats = {}

                -- Determine item slot, even on tokens:
                local isToken, tokenSlot
                _, _, isToken, tokenSlot = string.find(uItem:GetItemTypeName(), "^(Token) %- .+ %- (%a+)$")
                if isToken and tItemQualities[uItem:GetItemQuality()].label == "Legendary" then
                    if      tokenSlot == "Chest"    then tItemInfo.eSlot = 0
                    elseif  tokenSlot == "Legs"     then tItemInfo.eSlot = 1
                    elseif  tokenSlot == "Head"     then tItemInfo.eSlot = 2
                    elseif  tokenSlot == "Shoulder" then tItemInfo.eSlot = 3
                    elseif  tokenSlot == "Feet"     then tItemInfo.eSlot = 4
                    elseif  tokenSlot == "Hands"    then tItemInfo.eSlot = 5
                    else
                    end
                end

		for i=1,#(tItemInfo.arInnateProperties or {}) do
			local stat = tItemInfo.arInnateProperties[i]
			tItemInfo.tStats[stat.eProperty] = stat.nValue
		end

		for i=1,#(tItemInfo.arBudgetBasedProperties or {}) do
			local stat = tItemInfo.arBudgetBasedProperties[i]
			tItemInfo.tStats[stat.eProperty] = stat.nValue
		end

		return tItemInfo
	end
end

function MythLC.StatsString(item)
	local tStrStats = {}

	for stat,value in pairs(item.tStats) do
		table.insert(tStrStats, string.format("%.0f", value)..(tStats[stat] and tStats[stat].short or "?"))
	end

 	return table.concat(tStrStats," | ")
end

-----------------------------------------------------------------------------------------------
-- LootCouncil Chat Functions
-----------------------------------------------------------------------------------------------
function MythLC:OnChatMessage(channelCurrent, tMessage)

	if channelCurrent and (channelCurrent:GetType() == activeChannel or channelCurrent:GetType() == testChannel) then
		for i, tSegment in ipairs( tMessage.arMessageSegments ) do

			if bCurrDistributing and tSpecList[tSegment.strText] ~= nil then -- spec read

				self:dPrint("[LC] Spec segment read: "..tSpecList[tSegment.strText])

				lastSpec = tSpecList[tSegment.strText]

				if lastItem ~= nil then
					self:dPrint("[LC] Item segment previously found, listing item")
					self:ListItem(lastItem, lastSpec, tMessage.strSender)
				end

			elseif bCurrDistributing and tSegment.uItem ~= nil then -- item link

				if bBeingDistributed then

					self:dPrint("[LC] Loot to be distributed read, calling function")
					self:OnSetCurrentItem(tSegment.uItem)
                    bBeingDistributed = false

				else
					self:dPrint("[LC] Linked item segment read")

					if lastSpec ~= "" then
						self:dPrint("[LC] Spec already set, listing item")
						self:ListItem(tSegment, lastSpec, tMessage.strSender)
					else
						self:dPrint("[LC] Spec not set yet, saving item and reading next segment")
						lastItem = tSegment
					end
				end

			elseif tSegment.strText == "Distributing " then

				bBeingDistributed = true
				self:OnDistributionStart(tMessage.strSender)

			elseif tSegment.strText == "Ended!" then

				self:OnDistributionStop()

			end

		end -- segment loop
	end -- Channel check
end

---------------------------------------------------------------------------------------------------
-- MasterLooter Assign Items
---------------------------------------------------------------------------------------------------

function MythLC:OnAssignToUnit( wndHandler, wndControl, eMouseButton )
    self:dPrint("OnAssignToUnit called")
    if not tItemList then return end

    if bIsLeader then
		uLooter = wndControl:GetData()
        if uLooter == nil then return end
        sCurrUnit = uLooter:GetName()

		self.wndMain:FindChild("LootDistDiag"):Invoke()
	end
end

function MythLC:OnAssignLoot( wndHandler, wndControl, eMouseButton )
    self:dPrint("OnAssignLoot called")
    if not tItemList then return end

    if bIsLeader then
		sCurrUnit = wndControl:FindChild("CharacterName"):GetText()
        uLooter = nil

		self.wndMain:FindChild("LootDistDiag"):Invoke()
	end
end

function MythLC:OnConfirmLootDistribution( wndHandler, wndControl, eMouseButton )
    self:dPrint("Sending Item: " .. tItemList[nLootIdx]["itemDrop"]:GetName() .. " to player: " .. sCurrUnit)

    if uLooter ~= nil then
        GameLib.AssignMasterLoot(tItemList[nLootIdx]["nLootId"], uLooter)
    else
        for _, rRollItem in pairs(tItemList) do
            for _, unit in pairs(rRollItem["tLooters"]) do
                if unit:GetName() == sCurrUnit then uLooter = unit end
            end
        end
        GameLib.AssignMasterLoot(tItemList[nLootIdx]["nLootId"], uLooter)
    end

	wndControl:GetParent():Close()

	if bDistributing then
		self.wndLeftSide:DestroyChildren()
		self.wndItemList:DestroyChildren()
        nLootIdx = nil
        currItem = nil
        sCurrUnit = ""
        uLooter = nil
		self:OnStopDistribution()
		self:OnMasterLootUpdate()
	else
		self.wndLeftSide:DestroyChildren()
		self.wndItemList:DestroyChildren()
        nLootIdx = nil
        currItem = nil
        sCurrUnit = ""
        uLooter = nil
		self:OnMasterLootUpdate()
	end
end

function MythLC:OnCancelLootDistribution( wndHandler, wndControl, eMouseButton )
    sCurrUnit = ""
	wndControl:GetParent():Close()
end

function MythLC:OnRandomize( wndHandler, wndControl, eMouseButton )
    self:dPrint("Randomize button clicked")

    local sRandomUnit = ""
    for _, rRollItem in pairs(tItemList) do
        sRandomUnit = rRollItem["tLooters"][math.random(#rRollItem["tLooters"])]
        self:dPrint("Random unit selected: " .. sRandomUnit:GetName() .. ", loot ID: " .. rRollItem.nLootId)
        GameLib.AssignMasterLoot(rRollItem.nLootId, sRandomUnit)
    end

    self:dPrint("All items randomized")

    self:OnMasterLootUpdate{}
end

function MythLC:OnLootAssigned(tLootInfo)
	if not tLootInfo then return end
	if not tLootInfo.itemLoot then return end

	local strItem = tLootInfo.itemLoot:GetChatLinkString()
	local nCount = tLootInfo.itemLoot:GetStackCount()
	if nCount > 1 then
		strItem = String_GetWeaselString(Apollo.GetString("CombatLog_MultiItem"), nCount, strItem)
	end

	Event_FireGenericEvent("GenericEvent_LootChannelMessage", String_GetWeaselString(Apollo.GetString("CRB_MasterLoot_AssignMsg"), strItem, tLootInfo.strPlayer))
end

function MythLC:OnAutoloot()
    if not tSettings.bAutoLoot then return end
    if not tSettings.sAutoLooter then
        Print("No AutoLooter set, please type in one in the Settings")
        return
    end

    tAutolootItems = {}
    for i, tAutoItem in pais(tItemList) do
        local strName = tAutoItem.itemDrop:GetName()
        for _, strItem in pairs(tSettings.tAutolootItems) do
            if strItem == strName then
                table.insert(tAutolootItems, tAutoItem)
            end
        end
    end

    uAutoLooter = nil
    for i = 1, GameLib:GetMemberCount() do
        if GameLib:GetUnitForGroupMember(i):GetName() == tSettings.sAutoLooter then
            uAutoLooter = GameLib:GetUnitForGroupMember(i)
        end
    end
    if uAutoLooter == nil then
        self:dPrint("AutoLooter not in group. Skipping autoloot")
        return
    end

    for i, tAutoItem in pais(tAutolootItems) do
        self:dPrint("Autolooted item: " .. strItem .. " to: " .. tSettings.sAutoLooter)
        GameLib.AssignMasterLoot(tAutoItem.nLootId, uAutoLooter)
    end

    self:OnMasterLootUpdate()
end
-----------------------------------------------------------------------------------------------
-- Debug functions
-----------------------------------------------------------------------------------------------
function MythLC:dPrint(msg)
	if tSettings.bDebug then Print("[MLC] " .. msg) end
end

-----------------------------------------------------------------------------------------------
-- MythLC Instance
-----------------------------------------------------------------------------------------------
local MythLCInst = MythLC:new()
MythLCInst:Init()
